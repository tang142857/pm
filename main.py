# -*- coding: utf-8 -*-
"""
Periodical manager 期刊管理程序

date: 2023/8/20
author: SiHeng Tang
file: main.py
copyright(c) DFSA Software Developers
此程序不能提供任何担保 WITHOUT WARRANTY OF ANY KIND
"""
from libpm import *

if __name__ == '__main__':
    application = app.Application()
    application.run()
