Periodical Manager | 期刊管理器
===
类似包管理器的爬虫框架，主要注重轻量和可持续管理。

# 使用 | USAGE
PM使用类似于包管理器的CLI接口进行控制标准的指令如下
```shell
python3 main.py <DO_TASK> <EXTENSION_NAME> <TARGET> [<EXT_0> <EXT_1> ...]
```

# 前言 | PREFIX
PM框架只实现最基本的功能，**所有**功能的实现都是通过扩展自身完成  
PM框架目的是为了方便爬虫的开发重复造轮子  
PM框架的扩展是完全自由的，PM的存在是为了服务于扩展而不是限制扩展但是这也表明  
**PM不能为你的使用提供任何担保，请明确你的所有操作并只从信任的途径获取扩展**
**PM的扩展都是通过网络途径获取，开发者不能为其提供任何担保或许可，使用带来的后果需要由使用者承担**

# 扩展 | EXTENSION
PM的功能都是扩展实现的，对于各个扩展请参见[扩展README](extension/README.md)

# 开发 | DEV
PM扩展的开发是容易的，具体的事项参见[PM开发文档](doc/DevREADME.md)  

## 分支
+ master - 主线和版本发布
+ dev - 核心框架开发
+ ext - 提供扩展的集成
