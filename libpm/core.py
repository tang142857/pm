"""
PM 核心抽象定义

date: 2023/8/20
author: SiHeng Tang
file: core.py
copyright(c) DFSA Software Developers
此程序不能提供任何担保 WITHOUT WARRANTY OF ANY KIND
"""

###############################
# 常量定义
###############################
EXIT_USAGE_ERR = 1
EXIT_METHOD_ERR = 2

LOG_LV_DEBUG = 0b0000
LOG_LV_INFO = 0b0001
LOG_LV_WARN = 0b0010
LOG_LV_ERR = 0b0100


###############################
# 基类定义
###############################
class PObject(object):
    pass


class PError(BaseException):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        for key in kwargs.keys():
            setattr(self, str(key), kwargs[key])

    def __str__(self):
        string = ''

        for attr in dir(self):
            if not attr.startswith('_'):
                string += f'{attr}={getattr(self, attr)} '

        return string


class PExtensionError(PError):
    pass


class PArgument(PObject):

    def __getitem__(self, item):
        return super().__getitem__(item)


class PApplication(PObject):
    """
    PM应用基类
        PM应用都派生此类，并重写此类中的方法
    基本属性
        argv
    基本方法
        get_top_dir
    应用方法
        message 打印接口
        log 程序日志接口
        dialog 对话接口
        progress 进度条
        image 图片展示
    """

    def __init__(self):
        self.argv: PArgument = None

    def run(self) -> None:
        pass

    def get_top_dir(self) -> str:
        pass

    def msg(self, *args, **kwargs):
        pass

    def log(self, string: str, level: int):
        pass

    def dialog(self, *args, **kwargs):
        pass

    def progress(self, percent: float):
        pass

    def image(self):
        pass

    def get_service(self, product: str):
        pass


class PService(PObject):
    def __init__(self, app: PApplication):
        self._app = app


class PFactory(PObject):
    """
    PM对象工厂基类
    """

    def __init__(self, app: PApplication):
        self._app = app

    def get_product(self, product: str) -> PService:
        pass


###############################
# 扩展定义
###############################
def force_override(func):
    """
    强制重写修饰器
    """

    def reject_execute(obj, *args, **kwargs):
        raise PExtensionError(*args, info='Function has not override')

    return reject_execute


class BaseExtension(object):
    """
    PM 扩展基类
    不建议重写构造函数
    接口函数
        接口函数以 do_ 开头，负责实现对应方法的功能，基类提供了一部分可供参考的模版，
        全部实现或者部分实现这些函数即可，同时可以扩展函数
    非接口函数
        非接口函数以下划线开头
    """

    def __init__(self, app: PApplication):
        self._app = app

    @force_override
    def do_CREATE(self):
        pass

    @force_override
    def do_UPDATE(self):
        pass

    @force_override
    def do_UPGRADE(self):
        pass

    @force_override
    def do_OUTPUT(self):
        pass

    @force_override
    def do_FORGET(self):
        pass
