"""
PM 框架初始化

date: 2023/8/20
author: SiHeng Tang
file: __init__.py
copyright(c) DFSA Software Developers
此程序不能提供任何担保 WITHOUT WARRANTY OF ANY KIND
"""
import sys

# 添加路径到 PATH 变量
sys.path.append(__path__[0])

__all__ = ['app', 'core', 'service']
